import React from "react";
import "./App.scss";
import Gallery from "./components/gallery/gallery";

function App() {
  return <Gallery />;
}

export default App;
